import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    apply_now_form: {
        firstname:'',
        lastname:'',
        dob:'',
        gender:'',
        nationality:'',
        country:'',
        email:'',
        contact_no:'',
    },
    q1:'',
    q2:'',
    q3:'',
    q4:'',
    q5:'',
    q6:'',
    q7:'',
    q8:'',
    q9:'',
    userInfo:true,
    step_one:false,
    step_two:false,
    step_three:false,
    step_four:false,
    step_five:false,
    step_six:false,
    step_seven:false,
    step_eight:false,
    step_nine:false,
    progress:0,
  },
  mutations: {
    ADD_USER_INFO(state,value){
        state.apply_now_form = value
    },
    ADD_Q1(state,value){
        state.q1 = value
    },
    ADD_Q2(state,value){
        state.q2 = value
    },
    ADD_Q3(state,value){
        state.q3 = value
    },
    ADD_Q4(state,value){
        state.q4 = value
    },
    ADD_Q5(state,value){
        state.q5 = value
    },
    ADD_Q6(state,value){
        state.q6 = value
    },
    ADD_Q7(state,value){
        state.q7 = value
    },
    ADD_Q8(state,value){
        state.q8 = value
    },
    ADD_Q9(state,value){
        state.q9 = value
    },
    ENABLE_USER_INFO(state){
        state.userInfo=true
        state.step_one=false
        state.step_two=false
        state.step_three=false
        state.step_four=false
        state.step_five=false
        state.step_six=false
        state.step_seven=false
        state.step_eight=false
        state.step_nine=false
    },
    ENABLE_STEP_ONE(state){
        state.userInfo=false
        state.step_one=true
        state.step_two=false
        state.step_three=false
        state.step_four=false
        state.step_five=false
        state.step_six=false
        state.step_seven=false
        state.step_eight=false
        state.step_nine=false
    },
    ENABLE_STEP_TWO(state){
        state.userInfo=false
        state.step_one=false
        state.step_two=true
        state.step_three=false
        state.step_four=false
        state.step_five=false
        state.step_six=false
        state.step_seven=false
        state.step_eight=false
        state.step_nine=false
    },
    ENABLE_STEP_THREE(state){
        state.userInfo=false
        state.step_one=false
        state.step_two=false
        state.step_three=true
        state.step_four=false
        state.step_five=false
        state.step_six=false
        state.step_seven=false
        state.step_eight=false
        state.step_nine=false
    },
    ENABLE_STEP_FOUR(state){
        state.userInfo=false
        state.step_one=false
        state.step_two=false
        state.step_three=false
        state.step_four=true
        state.step_five=false
        state.step_six=false
        state.step_seven=false
        state.step_eight=false
        state.step_nine=false
    },
    ENABLE_STEP_FIVE(state){
        state.userInfo=false
        state.step_one=false
        state.step_two=false
        state.step_three=false
        state.step_four=false
        state.step_five=true
        state.step_six=false
        state.step_seven=false
        state.step_eight=false
        state.step_nine=false
    },
    ENABLE_STEP_SIX(state){
        state.userInfo=false
        state.step_one=false
        state.step_two=false
        state.step_three=false
        state.step_four=false
        state.step_five=false
        state.step_six=true
        state.step_seven=false
        state.step_eight=false
        state.step_nine=false
    },
    ENABLE_STEP_SEVEN(state){
        state.userInfo=false
        state.step_one=false
        state.step_two=false
        state.step_three=false
        state.step_four=false
        state.step_five=false
        state.step_six=false
        state.step_seven=true
        state.step_eight=false
        state.step_nine=false
    },
    ENABLE_STEP_EIGHT(state){
        state.userInfo=false
        state.step_one=false
        state.step_two=false
        state.step_three=false
        state.step_four=false
        state.step_five=false
        state.step_six=false
        state.step_seven=false
        state.step_eight=true
        state.step_nine=false
    },
    ENABLE_STEP_NINE(state){
        state.userInfo=false
        state.step_one=false
        state.step_two=false
        state.step_three=false
        state.step_four=false
        state.step_five=false
        state.step_six=false
        state.step_seven=false
        state.step_eight=false
        state.step_nine=true
    },
    SET_PROGRESS(state,value){
        state.progress = value
    }
  },
  actions: {
    AddUserInfo({commit},value){
        commit("ADD_USER_INFO", value)
    },
    AddQ1({commit},value){
        commit("ADD_Q1", value)
    },
    AddQ2({commit},value){
        commit("ADD_Q2", value)
    },
    AddQ3({commit},value){
        commit("ADD_Q3", value)
    },
    AddQ4({commit},value){
        commit("ADD_Q4", value)
    },
    AddQ5({commit},value){
        commit("ADD_Q5", value)
    },
    AddQ6({commit},value){
        commit("ADD_Q6", value)
    },
    AddQ7({commit},value){
        commit("ADD_Q7", value)
    },
    AddQ8({commit},value){
        commit("ADD_Q8", value)
    },
    AddQ9({commit},value){
        commit("ADD_Q9", value)
    },

    // ========================================= //

    EnableUserInfo({ commit }){
        commit("ENABLE_USER_INFO")
    },
    EnableStepOne({commit}){
        commit("ENABLE_STEP_ONE")
    },
    EnableStepTwo({commit}){
        commit("ENABLE_STEP_TWO")
    },
    EnableStepThree({commit}){
        commit("ENABLE_STEP_THREE")
    },
    EnableStepFour({commit}){
        commit("ENABLE_STEP_FOUR")
    },
    EnableStepFive({commit}){
        commit("ENABLE_STEP_FIVE")
    },
    EnableStepSix({commit}){
        commit("ENABLE_STEP_SIX")
    },
    EnableStepSeven({commit}){
        commit("ENABLE_STEP_SEVEN")
    },
    EnableStepEight({commit}){
        commit("ENABLE_STEP_EIGHT")
    },
    EnableStepNine({commit}){
        commit("ENABLE_STEP_NINE")
    },

    // ==================================== //

    CompleteApplication(){

    },
  },
  modules: {
  }
})
